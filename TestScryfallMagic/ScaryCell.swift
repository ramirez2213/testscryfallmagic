//
//  ScaryCell.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

protocol FeedCellViewModel {
    
    var text: String? { get }
    var photoAttachements: FeedCellPhotoAttachementViewModel? { get }
    var sizes: FeedCellSizes { get }
}

protocol FeedCellSizes {
    var postLabelFrame: CGRect { get }
    var attachmentFrame: CGRect { get }
    var bottomViewFrame: CGRect { get }
    var totalHeight: CGFloat { get }
}

protocol FeedCellPhotoAttachementViewModel {
    var photoUrlString: String? { get }
}

class ScaryCell: UITableViewCell {
    
    static let reuseId = "ScaryCell"
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var postImageView: WebImageView!
    
    override func prepareForReuse() {
      postImageView.set(imageURL: nil)
}

    override func awakeFromNib() {
        super.awakeFromNib()
        
        cardView.layer.cornerRadius = 10
        cardView.clipsToBounds = true
        
        backgroundColor = .clear
        selectionStyle = .none
    }

    func set(viewModel: FeedCellViewModel) {
       postLabel.text = viewModel.text!
        postImageView.set(imageURL: viewModel.photoAttachements?.photoUrlString)
        
        postLabel.frame = viewModel.sizes.postLabelFrame
        postImageView.frame = viewModel.sizes.attachmentFrame
    }
    
}
