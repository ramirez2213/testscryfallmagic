//
//  ScryfallCellLayoutcalcullator.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

struct Sizes: FeedCellSizes {
    var postLabelFrame: CGRect
    var attachmentFrame: CGRect
    var totalHeight: CGFloat
    var bottomViewFrame: CGRect
    
}

protocol FeedCellLayoutCalculatorProtocol {
    func sizes(postText: String?, photoAttachments: FeedCellPhotoAttachementViewModel? ) -> FeedCellSizes
}

final class ScryfallCellLayoutcalcullator: FeedCellLayoutCalculatorProtocol {
    
    private let screenWidth: CGFloat
    
    init(screenWidth: CGFloat = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)) {
        self.screenWidth = screenWidth
    }
    
    func sizes(postText: String?, photoAttachments: FeedCellPhotoAttachementViewModel?) -> FeedCellSizes {
        
        let cardViewWidth = screenWidth - Constant.cardInsets.left - Constant.cardInsets.right
        
        // MARK: Работа с postLabelFrame
        
        var postLabelFrame = CGRect(origin: CGPoint(x: Constant.postLabelInsets.left, y: Constant.postLabelInsets.top),
                                    size: CGSize.zero)
        
        if let text = postText, !text.isEmpty {
            let width = cardViewWidth - Constant.postLabelInsets.left - Constant.postLabelInsets.right
            let height = text.height(width: width, font: Constant.postLabelFont)
            postLabelFrame.size  = CGSize(width: width, height: height)
        }
        
        // MARK: Работа с attachmentFrame
        
        let attachmentTop = postLabelFrame.size == CGSize.zero ? Constant.postLabelInsets.top : postLabelFrame.maxY + Constant.postLabelInsets.bottom
        
        var attachmentFrame = CGRect(origin: CGPoint(x: 0, y: attachmentTop), size: CGSize.zero)
        
        let photoHeight: Float = Float(700)
        let photoWidth: Float = Float(500)
        let ratio = CGFloat(photoHeight / photoWidth)
        
        attachmentFrame.size = CGSize(width: cardViewWidth, height: cardViewWidth * ratio)
        
        // MARK: Работа с bottomViewFrame
        
        let bottomViewTop = max(postLabelFrame.maxY, attachmentFrame.maxY)
        
        let bottomViewFrame = CGRect(origin: CGPoint(x: 0, y: bottomViewTop), size: CGSize(width: cardViewWidth, height: 0))
        
        // MARK: Работа с totalHeight
        
        let totalHeight = bottomViewFrame .maxY + Constant.cardInsets.bottom
        
        return Sizes(postLabelFrame: postLabelFrame,
                     attachmentFrame: attachmentFrame,
                     totalHeight: totalHeight,
                     bottomViewFrame: bottomViewFrame)
    }
    
}
