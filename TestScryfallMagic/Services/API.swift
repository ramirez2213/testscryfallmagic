//
//  API.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct API {
    static let scheme = "https"
    static let host = "api.scryfall.com"
    static let scryfallList = "/cards/search"
}

