//
//  FeedResponse.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import Foundation

struct FeedResponse: Decodable {
    var data: [Datum]
}
struct Datum: Decodable {
    var oracleText: String?
    var imageUris: Image?
}

struct Image: Decodable {
    var small: String?
}
