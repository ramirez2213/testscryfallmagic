//
//  NetworkService.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import Foundation

protocol Networking {
    //get data
    func request(searchText:String,path: String, completion: @escaping (Data?, Error?) -> Void)
}

final class NetworkService: Networking {
    
    func request(searchText:String,path: String, completion: @escaping (Data?, Error?) -> Void) {
        let params:[String : Any] = ["q":"\(searchText)"]
        let url = self.url(from: path, params: params as! [String : String])
        let request = URLRequest(url: url)
        let task = createDataTask(from: request, completion: completion)
        print(url)
        task.resume()
    }
    
    private func createDataTask(from request: URLRequest, completion:  @escaping (Data?, Error?) -> Void) -> URLSessionDataTask{
        return URLSession.shared.dataTask(with: request) { (data , response, error) in
            DispatchQueue.main.async {
                completion(data, error)
            }
        }
    }
    
    private func url (from patch: String, params: [String: String]) -> URL {
        var components = URLComponents()
        
        components.scheme = API.scheme
        components.host = API.host
        components.path = patch
        components.queryItems = params.map {  URLQueryItem(name: $0, value: $1) }
        return components.url!
    }
}
