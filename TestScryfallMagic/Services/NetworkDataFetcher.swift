//
//  NetworkDataFetcher.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 31.01.2020.
//  Copyright © 2020 Konstantin Chukhas. All rights reserved.
//

import Foundation

protocol DataFetcher {
    func getFeed(searchText:String, response: @escaping (FeedResponse?) -> Void)
}

struct NetworkDataFetcher: DataFetcher {
    
    let networking:Networking
    
    init(networking: Networking) {
        self.networking = networking
        
    }
    
    func getFeed(searchText:String, response: @escaping (FeedResponse?) -> Void) {
        networking.request(searchText: searchText, path: API.scryfallList) { (data, error) in
            if let error = error {
                print("Error recived requesting data: \(error.localizedDescription)")
                response(nil)
            }
            let decoded = self.decodeJson(type: FeedResponse.self, from: data)
            response(decoded)
            
        }
    }
    
    private func decodeJson<T: Decodable>(type: T.Type, from: Data!) -> T? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        guard let data = from, let response = try? decoder.decode(type.self, from: data) else { return nil }
        return response
    }
}
