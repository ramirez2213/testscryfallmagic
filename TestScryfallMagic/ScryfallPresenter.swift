//
//  ScryfallPresenter.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 30.01.2020.
//  Copyright (c) 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

protocol ScryfallPresentationLogic {
    func presentData(response: Scryfall.Model.Response.ResponseType)
}

class ScryfallPresenter: ScryfallPresentationLogic {
    weak var viewController: ScryfallDisplayLogic?
    var cellLayoutCalculator: FeedCellLayoutCalculatorProtocol =  ScryfallCellLayoutcalcullator()
    
    func presentData(response: Scryfall.Model.Response.ResponseType) {
        switch response{
        case .presentNewsFeed(let feed):
            
            let cells = feed.data.map { (datum)  in
                cellViewModel(from: datum)
            }
            
            let feedViewModel = FeedViewModel.init(cells: cells)
            viewController?.displayData(viewModel: Scryfall.Model.ViewModel.ViewModelData.displayNewsfeed(feedViewModel: feedViewModel))
        }
    }
    
    private func cellViewModel(from feed: Datum) -> FeedViewModel.Cell {
        let photoAttachment = self.photoAttachments(feedItem: feed)
        
        let sizes = cellLayoutCalculator.sizes(postText: feed.oracleText, photoAttachments: photoAttachment ?? FeedViewModel.FeedCellPhotoAttachment.init(photoUrlString: ""))
        return FeedViewModel.Cell.init( photoAttachements: photoAttachment, sizes: sizes,
                                        text: String(feed.oracleText ?? ""))
    }
    
    private func photoAttachments(feedItem: Datum) -> FeedViewModel.FeedCellPhotoAttachment? {
        guard let photo = feedItem.imageUris?.small else { return nil}
        return FeedViewModel.FeedCellPhotoAttachment.init(photoUrlString: photo)
    }
    
}
