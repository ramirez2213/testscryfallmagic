//
//  ScryfallModels.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 30.01.2020.
//  Copyright (c) 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

enum Scryfall {
    
    enum Model {
        struct Request {
            enum RequestType {
                case getSearchFeeds(SearchText:String)
            }
        }
        struct Response {
            enum ResponseType {
                case presentNewsFeed(feed: FeedResponse)
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayNewsfeed(feedViewModel: FeedViewModel)
            }
        }
    }
}
struct FeedViewModel {
    struct Cell: FeedCellViewModel {
        
        var photoAttachements: FeedCellPhotoAttachementViewModel?
        var sizes: FeedCellSizes
        var text: String?
    }
    
    struct FeedCellPhotoAttachment: FeedCellPhotoAttachementViewModel {
        var photoUrlString: String?
    }
    
    let cells: [Cell]
    
}


