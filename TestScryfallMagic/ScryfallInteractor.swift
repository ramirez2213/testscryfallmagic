//
//  ScryfallInteractor.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 30.01.2020.
//  Copyright (c) 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

protocol ScryfallBusinessLogic {
  func makeRequest(request: Scryfall.Model.Request.RequestType)
}

class ScryfallInteractor: ScryfallBusinessLogic {

  var presenter: ScryfallPresentationLogic?
  var service: ScryfallService?
private var fetcher: DataFetcher = NetworkDataFetcher.init(networking: NetworkService())
  
  func makeRequest(request: Scryfall.Model.Request.RequestType) {
    if service == nil {
      service = ScryfallService()
    }
    
    switch request {
    case .getSearchFeeds(let SearchText):
        fetcher.getFeed(searchText: SearchText) { [weak self] (feedResponse) in
             guard let feedResponse = feedResponse else { return }
            self?.presenter?.presentData(response: Scryfall.Model.Response.ResponseType.presentNewsFeed(feed: feedResponse))
        }

    }
  }
  
}
