//
//  ScryfallViewController.swift
//  TestScryfallMagic
//
//  Created by Konstantin Chukhas on 30.01.2020.
//  Copyright (c) 2020 Konstantin Chukhas. All rights reserved.
//

import UIKit

protocol ScryfallDisplayLogic: class {
    func displayData(viewModel: Scryfall.Model.ViewModel.ViewModelData)
}

class ScryfallViewController: UIViewController, ScryfallDisplayLogic {
    
    var interactor: ScryfallBusinessLogic?
    var router: (NSObjectProtocol & ScryfallRoutingLogic)?
    private var feedViewModel = FeedViewModel.init(cells: [])
    private var  timer:Timer?
    var searchText = UserDefaults.standard.object(forKey: "search")
    
    @IBOutlet weak var table: UITableView!
    
    private var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        return refreshControl
    }()
    
    // MARK: Setup
    
    private func setup() {
        let viewController        = self
        let interactor            = ScryfallInteractor()
        let presenter             = ScryfallPresenter()
        let router                = ScryfallRouter()
        viewController.interactor = interactor
        viewController.router     = router
        interactor.presenter      = presenter
        presenter.viewController  = viewController
        router.viewController     = viewController
    }
    
    // MARK: Routing
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupTable()
        setupTopBars()
    }
    
    private func setupTable() {
        let topInset: CGFloat = 8
        table.contentInset.top = topInset
        
        table.register(UINib(nibName: "ScaryCell", bundle: nil), forCellReuseIdentifier: ScaryCell.reuseId)        
        table.separatorStyle = .none
        table.backgroundColor = .clear
        
        table.addSubview(refreshControl)
        
    }
    
    private func setupTopBars() {
        let searchController = UISearchController(searchResultsController: nil)
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        
    }
    
    @objc private func refresh() {
        //        interactor?.makeRequest(request: Scryfall.Model.Request.RequestType.getSearchFeeds("d"))
    }
    
    func displayData(viewModel: Scryfall.Model.ViewModel.ViewModelData) {
        switch viewModel {
            
        case .displayNewsfeed(let feedViewModel):
            self.feedViewModel = feedViewModel
            table.reloadData()
            
        }
    }
    
}

// MARK: - UITableViewDataSource

extension ScryfallViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return feedViewModel.cells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = table.dequeueReusableCell(withIdentifier: ScaryCell.reuseId, for: indexPath) as! ScaryCell
        let cellViewModel = feedViewModel.cells[indexPath.row]
        cell.set(viewModel: cellViewModel)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ScryfallViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellViewModel = feedViewModel.cells[indexPath.row]
        return cellViewModel.sizes.totalHeight
    }
}

// MARK: - UISearchBarDelegate

extension ScryfallViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: { [weak self] (_) in
            self?.interactor?.makeRequest(request: Scryfall.Model.Request.RequestType.getSearchFeeds(SearchText: searchText))
        })
    }
    
    
}
